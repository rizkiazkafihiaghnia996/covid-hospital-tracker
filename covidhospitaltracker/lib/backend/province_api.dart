import 'dart:convert';

import 'package:covidhospitaltracker/model/province_model.dart';
import 'package:http/http.dart' as http;

class ProvinceRepository {
  Future<ProvinceModel> getProvince() async {
    var response = await http.get(Uri.parse(
      'https://rs-bed-covid-api.vercel.app/api/get-provinces'
    ));

    if(response.statusCode == 200) {
      print(response.body);
      return ProvinceModel.fromJson(json.decode(response.body));
    }
    else {
      throw Exception(
        'Something Went Wrong'
      );
    }
  }
}