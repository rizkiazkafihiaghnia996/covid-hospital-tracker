import 'dart:convert';

HospitalDetailModel hospitalDetailModelFromJson(String str) => HospitalDetailModel.fromJson(json.decode(str));

String hospitalDetailModelToJson(HospitalDetailModel data) => json.encode(data.toJson());

class HospitalDetailModel {
    HospitalDetailModel({
        this.status,
        this.data,
    });

    int status;
    Data data;

    factory HospitalDetailModel.fromJson(Map<String, dynamic> json) => HospitalDetailModel(
        status: json["status"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": data.toJson(),
    };
}

class Data {
    Data({
        this.id,
        this.name,
        this.address,
        this.phone,
        this.bedDetail,
    });

    String id;
    String name;
    String address;
    String phone;
    List<BedDetail> bedDetail;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        name: json["name"],
        address: json["address"],
        phone: json["phone"],
        bedDetail: List<BedDetail>.from(json["bedDetail"].map((x) => BedDetail.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "address": address,
        "phone": phone,
        "bedDetail": List<dynamic>.from(bedDetail.map((x) => x.toJson())),
    };
}

class BedDetail {
    BedDetail({
        this.time,
        this.stats,
    });

    String time;
    Stats stats;

    factory BedDetail.fromJson(Map<String, dynamic> json) => BedDetail(
        time: json["time"],
        stats: Stats.fromJson(json["stats"]),
    );

    Map<String, dynamic> toJson() => {
        "time": time,
        "stats": stats.toJson(),
    };
}

class Stats {
    Stats({
        this.title,
        this.bedAvailable,
        this.bedEmpty,
        this.queue,
    });

    String title;
    int bedAvailable;
    int bedEmpty;
    int queue;

    factory Stats.fromJson(Map<String, dynamic> json) => Stats(
        title: json["title"],
        bedAvailable: json["bed_available"],
        bedEmpty: json["bed_empty"],
        queue: json["queue"],
    );

    Map<String, dynamic> toJson() => {
        "title": title,
        "bed_available": bedAvailable,
        "bed_empty": bedEmpty,
        "queue": queue,
    };
}
