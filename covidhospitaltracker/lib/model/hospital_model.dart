import 'dart:convert';

HospitalModel hospitalModelFromJson(String str) => HospitalModel.fromJson(json.decode(str));

String hospitalModelToJson(HospitalModel data) => json.encode(data.toJson());

class HospitalModel {
    HospitalModel({
        this.status,
        this.hospitals,
    });

    int status;
    List<Hospital> hospitals;

    factory HospitalModel.fromJson(Map<String, dynamic> json) => HospitalModel(
        status: json["status"],
        hospitals: List<Hospital>.from(json["hospitals"].map((x) => Hospital.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "hospitals": List<dynamic>.from(hospitals.map((x) => x.toJson())),
    };
}

class Hospital {
    Hospital({
        this.id,
        this.name,
        this.address,
        this.phone,
        this.queue,
        this.bedAvailability,
        this.info,
    });

    String id;
    String name;
    String address;
    String phone;
    int queue;
    int bedAvailability;
    String info;

    factory Hospital.fromJson(Map<String, dynamic> json) => Hospital(
        id: json["id"],
        name: json["name"],
        address: json["address"],
        phone: json["phone"],
        queue: json["queue"],
        bedAvailability: json["bed_availability"],
        info: json["info"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "address": address,
        "phone": phone,
        "queue": queue,
        "bed_availability": bedAvailability,
        "info": info,
    };
}
