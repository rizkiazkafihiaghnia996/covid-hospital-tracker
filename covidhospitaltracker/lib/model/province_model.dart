import 'dart:convert';

ProvinceModel provinceModelFromJson(String str) => ProvinceModel.fromJson(json.decode(str));

String provinceModelToJson(ProvinceModel data) => json.encode(data.toJson());

class ProvinceModel {
    ProvinceModel({
        this.provinces,
    });

    List<Province> provinces;

    factory ProvinceModel.fromJson(Map<String, dynamic> json) => ProvinceModel(
        provinces: List<Province>.from(json["provinces"].map((x) => Province.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "provinces": List<dynamic>.from(provinces.map((x) => x.toJson())),
    };
}

class Province {
    Province({
        this.id,
        this.name,
    });

    String id;
    String name;

    factory Province.fromJson(Map<String, dynamic> json) => Province(
        id: json["id"],
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
    };
}
