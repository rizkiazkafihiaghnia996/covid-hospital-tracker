import 'dart:convert';

HospitalLocationModel hospitalLocationModelFromJson(String str) => HospitalLocationModel.fromJson(json.decode(str));

String hospitalLocationModelToJson(HospitalLocationModel data) => json.encode(data.toJson());

class HospitalLocationModel {
    HospitalLocationModel({
        this.status,
        this.data,
    });

    int status;
    Data data;

    factory HospitalLocationModel.fromJson(Map<String, dynamic> json) => HospitalLocationModel(
        status: json["status"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "data": data.toJson(),
    };
}

class Data {
    Data({
        this.id,
        this.name,
        this.address,
        this.lat,
        this.long,
        this.gmaps,
    });

    String id;
    String name;
    String address;
    String lat;
    String long;
    String gmaps;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        name: json["name"],
        address: json["address"],
        lat: json["lat"],
        long: json["long"],
        gmaps: json["gmaps"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "address": address,
        "lat": lat,
        "long": long,
        "gmaps": gmaps,
    };
}
