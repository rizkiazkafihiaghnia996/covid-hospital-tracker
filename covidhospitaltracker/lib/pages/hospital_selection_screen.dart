import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:covidhospitaltracker/model/hospital_model.dart';
import 'package:http/http.dart' as http;

class HospitalScreen extends StatefulWidget {
  final String provinceID, cityID, cityName;

  const HospitalScreen({ Key key, this.provinceID, this.cityID, this.cityName }) : super(key: key);

  @override
  _HospitalScreenState createState() => _HospitalScreenState();
}

class _HospitalScreenState extends State<HospitalScreen> {
  Future<HospitalModel> getHospital() async {
    var response = await http.get(Uri.parse(
      'https://rs-bed-covid-api.vercel.app/api/get-hospitals?provinceid=' + widget.provinceID + '&cityid=' + widget.cityID + '&type=1'
    ));

    print(widget.provinceID);
    print(widget.cityID);
    if(response.statusCode == 200) {
      print(response.body);
      return HospitalModel.fromJson(json.decode(response.body));
    }
    else {
      throw Exception(
        'Something Went Wrong'
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red,
      appBar: AppBar(
        backgroundColor: Colors.red,
        iconTheme: IconThemeData(
          color: Colors.white
        ),
        //elevation: 0,
      ),
      body: Container(
        padding: const EdgeInsets.fromLTRB(16, 20, 16, 5),
        child: FutureBuilder(
          future: getHospital(),
          builder: (context, snapshot) {
            if (snapshot.data == null) {
              return Container(
                padding: const EdgeInsets.only(top: 270),
                alignment: Alignment.center,
                child: Column(
                  children: [
                    CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                    ),
                    SizedBox(height: 15),
                    Text(
                      'Mohon Tunggu Sebentar...',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                        fontWeight: FontWeight.bold
                      ),
                    )
                  ],
                ),
              );
            }
            else {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 1,
                    child: HospitalList(
                      hospital: snapshot.data,
                    ),
                  ),
                ],
              );
            }
          },
        ),
      ),
    );
  }
}

class HospitalList extends StatefulWidget {
  final HospitalModel hospital;

  const HospitalList({ Key key, this.hospital }) : super(key: key);

  @override
  _HospitalListState createState() => _HospitalListState();
}

class _HospitalListState extends State<HospitalList> {
  List<Hospital> hospitals;

  @override
  void initState() {
    hospitals = widget.hospital.hospitals;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: hospitals.length,
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () {

          },
          child: Container(
            margin: const EdgeInsets.only(top: 5, bottom: 5),
            padding: const EdgeInsets.fromLTRB(8, 20, 10, 20),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5)
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      hospitals[index].name,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                    Row(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(top: 5),
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.circular(10)
                          ),
                          child: Column(
                            children: [
                              Text(
                                "Kasur yang tersedia :",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold
                                ),
                              ),
                              Text(
                                hospitals[index].bedAvailability.toString(),
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                  color: Colors.white
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(width: 10),
                        Container(
                          margin: const EdgeInsets.only(top: 5),
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            color: Colors.orange,
                            borderRadius: BorderRadius.circular(10)
                          ),
                          child: Column(
                            children: [
                              Text(
                                "Jumlah Antrian :",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold
                                ),
                              ),
                              Text(
                                hospitals[index].queue.toString(),
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                  color: Colors.white
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
                Icon(
                  Icons.keyboard_arrow_right,
                  color: Colors.black,
                )
              ],
            ),
          ),
        );
      },
    );
  }
}