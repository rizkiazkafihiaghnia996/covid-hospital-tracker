import 'dart:convert';

import 'package:covidhospitaltracker/pages/hospital_selection_screen.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List provinceList;
  String selectedProvince;
  List citiesList;
  String selectedCity;

  Future<String> getProvince() async {
    await http.post(
      Uri.parse('https://rs-bed-covid-api.vercel.app/api/get-provinces'
    )).then((response) {
      var data = json.decode(response.body);
      print(data);
      setState(() {
        provinceList = data['provinces'];
      });
    });
  }

  Future<String> getCity(String id) async {
    await http.get(
      Uri.parse('https://rs-bed-covid-api.vercel.app/api/get-cities?provinceid=' + id
    )).then((response) {
      var data = json.decode(response.body);
      print(data);
      setState(() {
        citiesList = data['cities'];
      });
    });
  }
  
  @override
  void initState() {
    getProvince();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red,
      body: Container(
        padding: const EdgeInsets.fromLTRB(16, 30, 16, 5),
        child: Center(
          child: Container(
            height: 250,
            width: 400,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.white
            ),
            child: Column(
              children: <Widget>[
                Container(
                  width: 300,
                  padding: const EdgeInsets.only(top: 50),
                  child: DropdownButtonHideUnderline(
                    child: ButtonTheme(
                      alignedDropdown: true,
                      child: DropdownButton<String>(
                        value: selectedProvince,
                        isExpanded: true,  
                        hint: Text('PROVINSI'),
                        onChanged: (String newValue) {
                          setState(() {
                            selectedCity = null;
                            selectedProvince = newValue;
                            getCity(newValue);
                            print(selectedProvince);
                          });
                        },
                        items: provinceList?.map((item) {
                          return new DropdownMenuItem(
                            child: new Text(item['name']),
                            value: item['id'].toString(),
                          );
                        })?.toList() ?? [],
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  width: 300,
                  child: DropdownButtonHideUnderline(
                    child: ButtonTheme(
                      alignedDropdown: true,
                      child: DropdownButton<String>(
                        value: selectedCity,
                        isExpanded: true,
                        hint: Text('KOTA/KABUPATEN'),
                        onChanged: (String newValue) {
                          setState(() {
                            selectedCity = newValue;
                            print(selectedCity);
                          });
                        },
                        items: citiesList?.map((item) {
                          return new DropdownMenuItem(
                            child: new Text(item['name']),
                            value: item['id'].toString(),
                          );
                        })?.toList() ?? [],
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    if(selectedCity == null) {
                      return showDialog(
                        context: context, 
                        builder: (context) {
                          return AlertDialog(
                            backgroundColor: Colors.red,
                            content: Text(
                              'Tolong pilih sebuah Kota atau Kabupaten',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white
                              ),
                            ),
                          );
                        }
                      );
                    }
                    else {
                      Navigator.push(
                        context, 
                        MaterialPageRoute(
                          builder: (context) {
                            return HospitalScreen(
                              provinceID: selectedProvince,
                              cityID: selectedCity,
                            );
                          }
                        )
                      );
                    }
                  },
                  child: Container(
                    margin: const EdgeInsets.only(top: 20),
                    padding: const EdgeInsets.all(16),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.red
                    ),
                    child: Text(
                      'CARI',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16
                      ),
                    ),
                  )
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}