import 'package:covidhospitaltracker/pages/home_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Covid Hospital Tracker',
      theme: ThemeData(
        primarySwatch: Colors.red
      ),
      home: HomeScreen(),
    );
  }
}